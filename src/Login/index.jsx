import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const Login = () => {
  const handleSubmit = (e) => {
    e.preventDefault();
    let email = e.target.email.value;
    let password = e.target.password.value;

    if (!email || !password) {
      return alert("Lengkapi data");
    }
    axios("https://localhost:3000/login", {
      method: "POST",
      header: {
        "Content-Type": "application/json",
      },
      data: {
        email: email,
        password: password,
      },
    })
      .then((res) => {
        console.log(res);
        if (res.status == 200) {
          localStorage.setItem("token", res.data.accessToken);
          localStorage.setItem("userData", JSON.stringify(res.data.user));
          window.location.href = "/";
          return;
        }
        //kalau gagal
        alert(res.data);
        console.error(res);
      })
      .catch((err) => {
        alert(err);
      });
  };
  return (
    <div className="w-full min-h-screen flex flex-col p-8 bg-black text-white">
      <h1 className="text-6xl font-bold">Login Sekarang</h1>
      <form
        className="w-full flex flex-col gap-4 mt-4"
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <div className="flex flex-col gap-2">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            placeholder="Masukan Email"
            name="email"
            id="email"
            required
            className="w-full h-12 rounded-full px-4 border-[1px] border-gray-200 outline-none text-black"
          />
        </div>
        <div className="flex flex-col gap-2">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            placeholder="Masukan Password"
            name="password"
            id="passord"
            required
            className="w-full h-12 rounded-full px-4 border-[1px] border-gray-200 outline-none text-black"
          />
        </div>
        <button
          type="submit"
          className="h-12 bg-pink-500 text-white rounded-full flex justify-center items-center"
        >
          Login
        </button>
      </form>
      <p className="mt-4">
        Belum mempunyai akun ?{" "}
        <Link to={"/register"} className="text-blue-500">
          Register Disini
        </Link>
      </p>
    </div>
  );
};
export default Login;
