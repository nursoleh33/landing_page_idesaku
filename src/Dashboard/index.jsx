import React from "react";
import { FiHome, FiList, FiPlusCircle, FiSettings } from "react-icons/fi";
const Dashboard = () => {
  const handleLogout = () => {
    localStorage.clear();
    window.location.href = "/";
  };
  return (
    <>
      <div className="w-full min-h-screen flex flex-col p-6 bg-slate-50 text-black">
        <nav className="w-full h-12 bg-black text-white sticky top-0 left-0 flex items-center px-3">
          <h1>List Belanja</h1>
        </nav>
      </div>
      <nav className="w-screen h-16 fixed flex bg-black text-white bottom-0 left-0">
        <span className="flex-1 h-full flex justify-center items-center text-white">
          <FiHome />
        </span>
        <span className="flex-1 h-full flex justify-center items-center text-white">
          <FiList />
        </span>
        <span className="flex-1 h-full flex justify-center items-center text-white">
          <FiPlusCircle />
        </span>
        <span className="flex-1 h-full flex justify-center items-center text-white">
          <FiSettings />
        </span>
      </nav>
    </>
  );
};
export default Dashboard;
