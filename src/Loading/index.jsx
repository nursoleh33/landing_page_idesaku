import React from "react";

const Homepage = () => {
  return (
    <div className="w-full h-screen flex flex-col justify-center item-center bg-orange-500 text-white">
      <p className="animate-bounce text-xl font-light">
        Loading Please wait...
      </p>
    </div>
  );
};
export default Homepage;
