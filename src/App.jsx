// import { useState } from "react";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import React, { useState, useEffect } from "react";
import Home from "./Home";
import Register from "./Register";
import Login from "./Login";
import Dashboard from "./Dashboard";
import Homepage from "./Loading";
// import LoadingScreen from "./LoadingScreen";

function App() {
  // const [load, setLoad] = useState(true);
  // const [isLogin, setIsLogin] = useState(false);

  // const [isLogin, setIsLogin] = useState(false);
  // useEffect(() => {
  //   //check apakah ada token d localStorage
  //   let token = localStorage.getItem("token");

  //   if (!token) {
  //     setLoad(false);
  //     setIsLogin(false);
  //   }
  //   setTimeout(() => {
  //     setLoad(false);
  //     setIsLogin(true);
  //   }, 1000);
  // }, []);

  // if (load) {
  //   return <Homepage />;
  // }
  // if (!load && isLogin) {
  //   return (
  //     <Routes>
  //       <Route path="/" element={<Dashboard />} />
  //       <Route path="*" element={<Dashboard />} />
  //     </Routes>
  //   );
  // }
  return (
    // screen seukuran layar
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/register" element={<Register />} />
      <Route path="/login" element={<Login />} />
      <Route path="*" element={<Home />} />
      <Route path="/home" element={<Dashboard />} />
    </Routes>
  );
}

export default App;
