import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className="w-full min-h-screen flex flex-col p-8">
      <h1 className="text-6xl font-bold">Aplikasi List Belanja</h1>
      <small className="text-lg mt-2">By Nur Muhamad Soleh</small>
      <div className="flex flex-col gap-2 mt-auto">
        <Link
          to="/register"
          className="h-12 flex bg-black rounded-full text-white  justify-center items-center uppercase"
        >
          Register Sekarang
        </Link>
        <Link
          to="/login"
          className="h-12 flex bg-orange-500 rounded-full text-white justify-center items-center uppercase"
        >
          Login Sekarang
        </Link>
      </div>
    </div>
  );
};
export default Home;
