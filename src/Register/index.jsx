import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
// import { useNavigate } from "react-router-dom";

const Register = () => {
  // let nav = useNavigate();
  const handleSubmit = (e) => {
    //start from dari reload
    e.preventDefault();
    let email = e.target.email.value;
    let password = e.target.password.value;
    let password2 = e.target.password2.value;

    if (!email || !password || !password2) {
      return alert("silakan lengkapi data");
    }
    if (password.length < 6) {
      return alert("minimal min 6 character");
    }
    if (password !== password2) {
      return alert("Password harus sama");
    }
    axios("http://localhost:3000/users", {
      method: "POST",
      header: {
        "Content-Type": "application/json",
      },
      data: {
        id: Date.now(),
        email: email,
        password: password,
      },
    })
      .then((res) => {
        if (res.status == 201) {
          localStorage.setItem("token", res.data.accessToken);
          localStorage.setItem("userData", JSON.stringify(res.data.user));
          //   console.log(res);
          //   nav("/", {
          //     replace: true,
          //   });
          window.location.href = "/login";
          return;
        }
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    // console.log({
    //   email,
    //   password,
    //   password2,
    // });
  };
  return (
    <div className="w-full min-h-screen flex flex-col p-8">
      <h1 className="text-6xl font-bold">Register Sekarang</h1>
      <form
        className="w-full flex flex-col gap-4 mt-4"
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <div className="flex flex-col gap-2">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            placeholder="Masukan Email"
            name="email"
            id="email"
            required
            className="w-full h-12 rounded-full px-4 border-[1px] border-gray-200 outline-none"
          />
        </div>
        <div className="flex flex-col gap-2">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            placeholder="Masukan Password"
            name="password"
            id="passord"
            required
            className="w-full h-12 rounded-full px-4 border-[1px] border-gray-200 outline-none"
          />
        </div>
        <div className="flex flex-col gap-2">
          <label htmlFor="password2">Re-Password</label>
          <input
            type="password"
            placeholder="Masukan Password"
            name="password2"
            id="passord2"
            required
            className="w-full h-12 rounded-full px-4 border-[1px] border-gray-200 outline-none"
          />
        </div>
        <button className="h-12 bg-black text-white rounded-full flex justify-center items-center">
          Register
        </button>
      </form>
      <p className="mt-4">
        Sudah mempunyai akun ?{" "}
        <Link to={"/login"} className="text-blue-500">
          Login Disini
        </Link>
      </p>
    </div>
  );
};
export default Register;
